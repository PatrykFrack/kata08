package dictionary.readable;

import dictionary.common.AbstractDictionaryServiceImplTest;
import dictionary.common.DictionaryService;

public class ReadableDictionaryServiceImplTest extends AbstractDictionaryServiceImplTest {

	@Override
	protected DictionaryService getDictionaryServiceIntance() {
		return new ReadableDictionaryServiceImpl(TEST_DICTIONARY_FILE_PATH);
	}
}
