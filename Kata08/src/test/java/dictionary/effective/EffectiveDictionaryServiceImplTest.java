package dictionary.effective;

import dictionary.common.AbstractDictionaryServiceImplTest;
import dictionary.common.DictionaryService;

public class EffectiveDictionaryServiceImplTest extends AbstractDictionaryServiceImplTest {

	@Override
	protected DictionaryService getDictionaryServiceIntance() {
		return new EffectiveDictionaryServiceImpl(TEST_DICTIONARY_FILE_PATH);
	}

}
