package dictionary.common;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

public abstract class AbstractDictionaryServiceImplTest {

	protected static final String TEST_DICTIONARY_FILE_PATH = "testDictionary.txt";
	private static final String STRING_FB_WITH_EQUAL_HASH = "FB";
	private static final String STRING_EA_WITH_EQUAL_HASH = "Ea";
	private static final String SECOND_STRING_FOR_EQUAL_HASH = "BAAA";
	private static final String FIRST_STRING = "ABC";
	private static final String SECOND_STRING = "DEF";
	private DictionaryService serviceUnderTest;

	@Before
	public void init() {
		serviceUnderTest = getDictionaryServiceIntance();
	}

	protected abstract DictionaryService getDictionaryServiceIntance();

	@Test
	public void checkIfDictionaryBeenCorrectlyInitialized() {
		Dictionary testDictionary = serviceUnderTest.getDictionary();
		HashMap<Long, Word> sixLetterWords = testDictionary.getSixLetterWords();
		HashSet<String> smallerWords = testDictionary.getSmallerThanSixLettersStrings();
		assertThat(sixLetterWords.size(), is(5));
		assertThat(smallerWords.size(), is(7));
	}

	@Test
	public void doFindWordInTestDictionary() {
		boolean result = serviceUnderTest.isWordFoundInDictionaryByConcatenatedString(FIRST_STRING, SECOND_STRING);
		assertThat(result, is(true));
	}

	@Test
	public void doNotfindWordInTestDictionary() {
		boolean result = serviceUnderTest.isWordFoundInDictionaryByConcatenatedString(SECOND_STRING, FIRST_STRING);
		assertThat(result, is(false));
	}

	@Test
	public void findAllWordsBasedOnConcatenationsInTestDictionary() {
		serviceUnderTest.printAllWordsBasedOnConcatenations();
	}

	/**
	 * comparing standard String hash value for "Ea" and "FB" would return equal
	 * values. So both String EaBAAA and FBBAAA would find the word FBBAAA. This
	 * test will check if only the correct concatenated String FB + BAAA will find a
	 * word.
	 */
	@Test
	public void checkIfOneOfTwoStringsWithEqualHashValuesWillFindAWordInDictionary() {
		boolean result = serviceUnderTest.isWordFoundInDictionaryByConcatenatedString(STRING_FB_WITH_EQUAL_HASH,
				SECOND_STRING_FOR_EQUAL_HASH);
		assertThat(result, is(false));
		result = serviceUnderTest.isWordFoundInDictionaryByConcatenatedString(STRING_EA_WITH_EQUAL_HASH,
				SECOND_STRING_FOR_EQUAL_HASH);
		assertThat(result, is(true));
	}

	@Test
	public void checkLongHashValueMethod() {
		Word wordFB = new Word(STRING_FB_WITH_EQUAL_HASH);
		Word wordEa = new Word(STRING_EA_WITH_EQUAL_HASH);
		long hashCodeFB = wordFB.longHashCode();
		long hashCodeEa = wordEa.longHashCode();
		assertNotEquals(hashCodeEa, hashCodeFB);
	}
}
