package dictionary.common;

/**
 * Interface used for Readable and Effective services. When creating new
 * instance a {@link Dictionary} is created with two collections containing
 * Strings from file.
 *
 */
public interface DictionaryService {

	/**
	 * File name of the wordlist mentioned in Kata08
	 */
	public static final String WORD_LIST_FILE_NAME = "wordList.txt";

	/**
	 * Creates concatenations of Stings and searches for 6 letter Strings from .txt
	 * file
	 */
	public void printAllWordsBasedOnConcatenations();

	/**
	 * Concatenate firstString and secondString and look for a matching word in
	 * dictionary
	 * 
	 * @param firstString
	 * @param secondString
	 */
	public boolean isWordFoundInDictionaryByConcatenatedString(String firstString, String secondString);

	/**
	 * @return Dictionary containing two collections: sixLetterWords and
	 *         smallerThanSixLettersStrings
	 */
	Dictionary getDictionary();
}
