package dictionary.common;

import java.util.HashMap;
import java.util.HashSet;

public class Dictionary {

	/**
	 * Length of Words in sixLetterWords collection
	 */
	public static final int SEARCHED_WORD_LENGTH = 6;

	/**
	 * Key is a modified hashValue of the six letter String; Value is {@link Word}
	 * wrapping a String
	 */
	private HashMap<Long, Word> sixLetterWords;

	/**
	 * Set containing all Strings that can be used for finding six letter words.
	 */
	private HashSet<String> smallerThanSixLettersStrings;

	public Dictionary() {
		initCollections();
	}

	private void initCollections() {
		sixLetterWords = new HashMap<Long, Word>();
		smallerThanSixLettersStrings = new HashSet<String>();
	}

	public void addWordToDictionary(String wordFromWordList) {
		if (wordFromWordList.length() < SEARCHED_WORD_LENGTH) {
			getSmallerThanSixLettersStrings().add(wordFromWordList);
		} else if (wordFromWordList.length() == SEARCHED_WORD_LENGTH) {
			Word wordObject = new Word(wordFromWordList);
			getSixLetterWords().put(wordObject.longHashCode(), wordObject);
		}
		// bigger words will be ignored
	}

	public Word getSixLetterWordByKey(long key) {
		return getSixLetterWords().get(key);
	}

	public HashMap<Long, Word> getSixLetterWords() {
		return sixLetterWords;
	}

	public HashSet<String> getSmallerThanSixLettersStrings() {
		return smallerThanSixLettersStrings;
	}

}
