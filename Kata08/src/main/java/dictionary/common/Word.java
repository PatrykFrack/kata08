package dictionary.common;

/**
 * 
 * This is a String wrapper that provides a wider spectrum o hashCodes than
 * String.
 *
 */
public class Word {

	private String word;
	private long hash;

	public Word(String word) {
		this.word = word;
	}

	public String getWord() {
		return word;
	}

	public static long getHashCodeOfConcatenatedStrings(String a, String b) {
		long hashCode = 0;
		hashCode = wordLongHashCode(a, hashCode);
		hashCode = wordLongHashCode(b, hashCode);
		return hashCode;
	}

	public long longHashCode() {
		long hashCode = hash;
		if (hashCode == 0) {
			hashCode = wordLongHashCode(word, hashCode);
			hash = hashCode;
		}
		return hashCode;
	}

	private static long wordLongHashCode(String string, long initialHashValue) {
		long result = initialHashValue;
		if (string != null && !string.isEmpty()) {
			for (int characterValue : string.toCharArray()) {
				result = result * 113 + characterValue;
			}
		}
		return result;
	}
}
