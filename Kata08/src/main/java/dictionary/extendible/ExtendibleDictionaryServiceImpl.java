
package dictionary.extendible;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

public class ExtendibleDictionaryServiceImpl implements ExtendibleDictionaryService {

	private static final String FILE_CODING = "UTF-8";
	private static final String RESURCES_PATH = "res/";

	private ExtendibleDictionary dictionary;

	public ExtendibleDictionaryServiceImpl(String dictionaryFileName, int searchedWordLength) {
		initDictionary(dictionaryFileName, searchedWordLength);
	}

	private void initDictionary(String dictionaryFileName, int searchedWordLength) {
		LineIterator lineIterator = null;
		try {
			if (dictionaryFileName == null || dictionaryFileName.isEmpty()) {
				throw new IllegalArgumentException("Dictionary file name is missing!");
			}
			File dictionaryFile = new File(RESURCES_PATH + dictionaryFileName);
			lineIterator = FileUtils.lineIterator(dictionaryFile, FILE_CODING);
			dictionary = new ExtendibleDictionary(searchedWordLength);
			lineIterator.forEachRemaining(word -> dictionary.addWordToDictionary(word));
		} catch (IOException | IllegalArgumentException e) {
			System.out.println("Oops, something went wrong! " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			if (lineIterator != null) {
				LineIterator.closeQuietly(lineIterator);
			}
		}
	}

	@Override
	public void printAllWordsBasedOnConcatenations() {
		HashSet<String> smallerThanSixLettersStrings = dictionary.getSmallerStrings();

		for (String firstSmallString : smallerThanSixLettersStrings) {
			for (String secondSmallString : smallerThanSixLettersStrings) {
				isWordFoundInDictionaryByConcatenatedString(firstSmallString, secondSmallString);
			}
		}
	}

	@Override
	public boolean isWordFoundInDictionaryByConcatenatedString(String firstString, String secondString) {
		if (isPossibleToFindAWordUsingGivenStrings(firstString, secondString)) {
			String fullLengthString = firstString.concat(secondString);
			boolean isStringFound = dictionary.getFullLengthStrings().contains(fullLengthString);
			if (isStringFound) {
				System.out.println(firstString + " + " + secondString + " => " + fullLengthString);
				return true;
			}
		}
		return false;
	}

	private boolean isPossibleToFindAWordUsingGivenStrings(String firstString, String secondString) {
		if (firstString == null || firstString.isEmpty() || secondString == null || secondString.isEmpty()) {
			return false;
		}
		return firstString.length() + secondString.length() == dictionary.getSearchedStringLength();
	}

	@Override
	public ExtendibleDictionary getDictionary() {
		return dictionary;
	}

}
