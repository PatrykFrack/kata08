package dictionary.extendible;

import java.util.HashSet;

public class ExtendibleDictionary {

	/**
	 * Length of Strings in fullLengthStrings collection
	 */
	private int searchedStringLength = 6;

	/**
	 * Collection in which we will look for concatenated Strings
	 */
	private HashSet<String> fullLengthStrings;

	/**
	 * Collection on which concatenated Strings will be based
	 */
	private HashSet<String> smallerStrings;

	public ExtendibleDictionary(int searchedWordLength) {
		if (searchedWordLength < 0) {
			throw new IllegalArgumentException("Cannot search for words with length < 0!");
		}
		this.searchedStringLength = searchedWordLength;
		initCollections();
	}

	private void initCollections() {
		fullLengthStrings = new HashSet<String>();
		smallerStrings = new HashSet<String>();
	}

	public void addWordToDictionary(String stringFromWordList) {
		if (stringFromWordList == null) {
			return; // Null values will be ignored
		}
		if (stringFromWordList.length() < searchedStringLength) {
			getSmallerStrings().add(stringFromWordList);
		} else if (stringFromWordList.length() == searchedStringLength) {
			getFullLengthStrings().add(stringFromWordList);
		}
		// bigger words will be ignored
	}

	public int getSearchedStringLength() {
		return searchedStringLength;
	}

	public void setSearchedWordLength(int searchedWordLength) {
		this.searchedStringLength = searchedWordLength;
	}

	public HashSet<String> getFullLengthStrings() {
		return fullLengthStrings;
	}

	public HashSet<String> getSmallerStrings() {
		return smallerStrings;
	}

}
