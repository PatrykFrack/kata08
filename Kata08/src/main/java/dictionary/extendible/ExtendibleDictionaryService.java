package dictionary.extendible;

/**
 * When creating new instance a {@link ExtendibleDictionary} is created with two
 * collections containing Strings from file.
 */
public interface ExtendibleDictionaryService {

	public static final String WORD_LIST_FILE_NAME = "wordList.txt";

	/**
	 * Searches for all strings based on two concatenated strings from .txt file
	 */
	public void printAllWordsBasedOnConcatenations();

	/**
	 * Concatenate firstString and secondString and look for a matching word in
	 * dictionary
	 * 
	 * @param firstString
	 * @param secondString
	 */
	public boolean isWordFoundInDictionaryByConcatenatedString(String firstString, String secondString);

	/**
	 * @return {@link ExtendibleDictionary} containing two collections:
	 *         fullLengthStrings and smallerStrings
	 */
	ExtendibleDictionary getDictionary();
}
