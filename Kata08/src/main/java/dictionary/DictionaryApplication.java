package dictionary;

import dictionary.common.DictionaryService;
import dictionary.effective.EffectiveDictionaryServiceImpl;
import dictionary.extendible.ExtendibleDictionaryService;
import dictionary.extendible.ExtendibleDictionaryServiceImpl;
import dictionary.readable.ReadableDictionaryServiceImpl;

public class DictionaryApplication {

	private DictionaryService effectiveDictionary;
	private DictionaryService readableDictionary;
	private ExtendibleDictionaryService extendibleDictionary;

	public static void main(String[] args) {
		DictionaryApplication application = new DictionaryApplication();

		// Effective Dictionary
		application.loadEffectiveDictionaryService();
		application.findAllWordsBasedOnConcatenationsInEffectiveDictionary();

		// Readable Dictionary
		application.loadReadableDictionary();
		application.findAllWordsBasedOnConcatenationsInReadableDictionary();

		// Extendible Dictionary
		application.loadExtendibleDictionary();
		application.findAllWordsBasedOnConcatenationsInExtendibleDictionary();
	}

	public void loadEffectiveDictionaryService() {
		long startTime = System.currentTimeMillis();
		effectiveDictionary = new EffectiveDictionaryServiceImpl(DictionaryService.WORD_LIST_FILE_NAME);
		long endTime = System.currentTimeMillis();
		System.out.println("Loading Effective dictionary took: " + (endTime - startTime) + " ms");
	}

	public void findAllWordsBasedOnConcatenationsInEffectiveDictionary() {
		long startTime = System.currentTimeMillis();
		effectiveDictionary.printAllWordsBasedOnConcatenations();
		long endTime = System.currentTimeMillis();
		System.out.println("Finding all words in Effective dictionary took: " + (endTime - startTime) + " ms");
	}

	public void loadReadableDictionary() {
		long startTime = System.currentTimeMillis();
		readableDictionary = new ReadableDictionaryServiceImpl(DictionaryService.WORD_LIST_FILE_NAME);
		long endTime = System.currentTimeMillis();
		System.out.println("Loading Readable dictionary took: " + (endTime - startTime) + " ms");
	}

	public void findAllWordsBasedOnConcatenationsInReadableDictionary() {
		long startTime = System.currentTimeMillis();
		readableDictionary.printAllWordsBasedOnConcatenations();
		long endTime = System.currentTimeMillis();
		System.out.println("Finding all words in Readable dictionary took: " + (endTime - startTime) + " ms");
	}

	public void loadExtendibleDictionary() {
		long startTime = System.currentTimeMillis();
		// 6 - length of searched words
		extendibleDictionary = new ExtendibleDictionaryServiceImpl(DictionaryService.WORD_LIST_FILE_NAME, 6);
		long endTime = System.currentTimeMillis();
		System.out.println("Loading Extendible dictionary took: " + (endTime - startTime) + " ms");
	}

	public void findAllWordsBasedOnConcatenationsInExtendibleDictionary() {
		long startTime = System.currentTimeMillis();
		extendibleDictionary.printAllWordsBasedOnConcatenations();
		long endTime = System.currentTimeMillis();
		System.out.println("Finding all words in Extendible dictionary took: " + (endTime - startTime) + " ms");
	}

}
