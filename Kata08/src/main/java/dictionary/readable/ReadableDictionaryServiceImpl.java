package dictionary.readable;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import dictionary.common.Dictionary;
import dictionary.common.DictionaryService;
import dictionary.common.Word;

public class ReadableDictionaryServiceImpl implements DictionaryService {

	private static final String RESURCES_PATH = "res/";

	private Dictionary dictionary;

	public ReadableDictionaryServiceImpl(String dictionaryFileName) {
		initDictionary(dictionaryFileName);
	}

	private void initDictionary(String dictionaryFileName) {
		File dictionaryFile = new File(RESURCES_PATH + dictionaryFileName);
		LineIterator lineIterator = null;
		try {
			lineIterator = FileUtils.lineIterator(dictionaryFile, "UTF-8");
			dictionary = new Dictionary();
			lineIterator.forEachRemaining(word -> dictionary.addWordToDictionary(word));
		} catch (IOException e) {
			System.out.println("Oops, something went wrong! " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			if (lineIterator != null) {
				LineIterator.closeQuietly(lineIterator);
			}
		}
	}

	@Override
	public void printAllWordsBasedOnConcatenations() {
		HashSet<String> smallerThanSixLettersStrings = dictionary.getSmallerThanSixLettersStrings();

		for (String firstSmallString : smallerThanSixLettersStrings) {
			for (String secondSmallString : smallerThanSixLettersStrings) {
				isWordFoundInDictionaryByConcatenatedString(firstSmallString, secondSmallString);
			}
		}
	}

	@Override
	public boolean isWordFoundInDictionaryByConcatenatedString(String firstString, String secondString) {
		if (isPossibleToFindAWordUsingGivenStrings(firstString, secondString)) {
			long searchedWordKey = Word.getHashCodeOfConcatenatedStrings(firstString, secondString);
			Word foundWord = dictionary.getSixLetterWordByKey(searchedWordKey);
			if (foundWord != null) {
				System.out.println(firstString + " + " + secondString + " => " + foundWord.getWord());
				return true;
			}
		}
		return false;
	}

	private boolean isPossibleToFindAWordUsingGivenStrings(String firstString, String secondString) {
		return firstString.length() + secondString.length() == Dictionary.SEARCHED_WORD_LENGTH;
	}

	@Override
	public Dictionary getDictionary() {
		return dictionary;
	}

}