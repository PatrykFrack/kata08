Author: Patryk Frąckowiak
mail: patryk.frackowiak90@gmail.com
phone number: +48 698 964 508

This was made using Eclipse Oxygen IDE and Java 1.8.
To start the application use the main method of DictionaryApplication.
To run this application:
	1. Create a new workspace
	2. Import Kata08 project as "Existing Projects into Workspace"
	3. Create new run configuration: Java Application -> On tab Main set Main class "dictionary.DictionaryApplication"

The Effective, Readable and Extendible programs use respectively EffectiveDictionaryServiceImpl, ReadableDictionaryServiceImpl and ExtendibleDictionaryServiceImpl.
